from PIL import Image
from rgb_xor_text import pull_bit, xor_end_bit


def check_text(path: str, length_find: int):
    """
    получает спрятанное сообщение

    :param path: путь к контейнеру
    :param length_find:
    :return: строку бит которая содержит сообщение
    """
    image = Image.open(path)
    width = image.size[0]
    height = image.size[1]
    pix = image.load()
    mess = ''
    for y in range(0, height, 2):
        y1 = y
        y1 += 1
        if y1 < height:
            for x in range(width):
                b, b1_bit, b_bit, g, g1_bit, g_bit, r, r1_bit, r_bit = pull_bit(pix, x, y, y1)
                xor = xor_end_bit(r_bit, r1_bit, g_bit, g1_bit, b_bit, b1_bit)
                mess += str(xor)
        else:
            break
    return mess[0:length_find]


if __name__ == '__main__':
    mess = check_text('p.png', 1)
    print(mess)
